// store.js

import Vue from 'vue';
import Vuex from 'vuex';
import moment from 'moment';
Vue.use(Vuex);
/* eslint-disable no-console */
const store = new Vuex.Store({
    state: {
        dataFilter: null,
        selecionados: [],
        count: 850,
        trafegoTotal: 0,
        combustivelTotal:0,
        totalProduzido: 0,
        areaColhida: 0,
        velocidadeMedia: 0,
        tempoParado: 0,
        tempoDisponivel: 0,
        tempoColhendo:0,
        loading: false,
        veiculos: [],
        totalp_prudtiva: [],
        fulldata: [],
        consumoMaquinarioGrafico: [],
        consumoMaquinarioGrafico2: {
            datasets: [],
            labels: []
        }
    },
    mutations: {
        setDataFilter(state, payload) {
            //console.log(payload)
            return state.dataFilter = payload
        },
        setSelecionados(state, payload) {
            //console.log(payload)
            return state.selecionados = payload
        },
        setLoading(state, payload) {
            return state.loading = payload
        },
        setVeiculos(state, payload) {
            var newData = [...new Set(payload.map(data => data.veiculo))]
            return state.veiculos = newData;
        },
        setFullData(state, payload) {

            /*if(state.selecionados.length == 0){
                state.combustivelTotal = 0;
                state.totalProduzido = 0;
                state.areaColhida = 0;
                state.velocidadeMedia = 0;
                state.tempoParado = 0;
                state.tempoDisponivel = 0;
                state.tempoColhendo = 0;
            }*/
            
            var newData = []
            var combustivelTotal = 0
            var totalProduzido= 0
            var areaColhida= 0
            var velocidadeMedia= 0
            var tempoParado= 0
            var tempoDisponivel= 0
            var tempoColhendo=0


            payload.map(item=>{
                if(state.dataFilter){
                    var dataItem = moment(item.data)
                    var data1 = moment(state.dataFilter[0]) 
                    var data2 = moment(state.dataFilter[1])
                    state.selecionados.forEach(selected => {
                        if(item.veiculo == selected)
                            if(dataItem > data1 && dataItem < data2)
                                newData.push(item)
                    })
                }else{
                    state.selecionados.forEach(selected => {
                        if(item.veiculo == selected)
                                newData.push(item)
                    })
                }
            });

            var consumoMaquinarioGrafico = []
            var p1 = [] 
            var p2 = []
            var totalp_prudtiva = []
            newData.forEach(item =>{
                combustivelTotal += Number(item.total_diesel);
                totalProduzido += Number(item.tonelada_colheita);
                areaColhida += Number(item.ha);
                velocidadeMedia += Number(item.media_velocidade);
                tempoParado += Number(item.motor_ocioso);
                tempoDisponivel += Number(item.p_disponibilidade);
                tempoColhendo += Number(item.horas_colheita);
                p1.push(item.data)
                p2.push(Math.floor(Number(item.total_diesel)))
                totalp_prudtiva.push(item.p_prudtiva)
            });
            consumoMaquinarioGrafico.push(p1)
            consumoMaquinarioGrafico.push(p2)

            state.consumoMaquinarioGrafico = consumoMaquinarioGrafico;
            state.consumoMaquinarioGrafico2 = {
                datasets: p2,
                labels: p1
            };
            state.totalp_prudtiva = totalp_prudtiva
            state.combustivelTotal = combustivelTotal;
            state.totalProduzido = totalProduzido;
            state.areaColhida = areaColhida;
            state.velocidadeMedia = (velocidadeMedia / payload.length) / state.selecionados.length;
            state.tempoParado = (tempoParado / payload.length) / state.selecionados.length;
            state.tempoDisponivel = (tempoDisponivel / payload.length) / state.selecionados.length;
            state.tempoColhendo = (tempoColhendo / payload.length) / state.selecionados.length; 
            state.fulldata = newData;
            return state.fulldata = newData;
        },
        increment(state, payload) {
            return state.count = state.count + payload.amount;
        }
    },
    actions: {
        setDataFilter(context, payload) {
            context.commit('setDataFilter', payload)
        },
        setSelecionados(context, payload) {
            context.commit('setSelecionados', payload)
        },
        setLoading(context, payload) {
            context.commit('setLoading', payload)
        },
        setFullData(context, payload) {
            context.commit('setFullData', payload)
        },
        setVeiculos(context, payload) {
            context.commit('setVeiculos', payload)
        },
        increment(context, payload) {
            context.commit('increment', payload)
        }
    }
})
export default store;