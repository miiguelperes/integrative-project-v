import axios from 'axios'

const instance = axios.create({
    baseURL: 'https://5b29996b84ce2c0014d4d185.mockapi.io/api/v1/registros'
    //baseURL: 'https://demo3686973.mockable.io/registros'
})
export default instance