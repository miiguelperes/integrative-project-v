import store from '@/store/store'
import axios from '@/service/api'
/* eslint-disable no-console */
export default () => {
    store.dispatch('setLoading', true)
    axios.get('').then(response => {
        var tableData = []
        response.data.forEach(element => {
            element.img = "img/theme/MAQ1.png";
            tableData.push(element)
          });
        //console.log(tableData)
        store.dispatch('setLoading', false)
        store.dispatch('setFullData', tableData)
        store.dispatch('setVeiculos', tableData)
    });
}